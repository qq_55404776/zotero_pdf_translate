# Zotero PDF翻译插件

## 简介
本仓库提供了一个用于Zotero的PDF翻译插件，版本为1.0.24，发布日期为2023年7月10日。该插件旨在帮助用户在阅读PDF文档时进行即时翻译，提升阅读效率和体验。

## 功能特点
- **即时翻译**：支持在阅读PDF文档时即时翻译选中的文本。
- **多语言支持**：支持多种语言的翻译，满足不同用户的需求。
- **易于集成**：作为Zotero插件，安装简便，与Zotero无缝集成。

## 安装指南
1. **下载插件**：从本仓库下载最新版本的插件文件（zotero-pdf-translate-1.0.24.xpi）。
2. **安装插件**：
   - 打开Zotero应用程序。
   - 点击工具栏中的“工具”菜单，选择“插件”。
   - 在插件管理器中，点击右上角的齿轮图标，选择“从文件安装插件...”。
   - 选择下载的插件文件（zotero-pdf-translate-1.0.24.xpi），点击“打开”进行安装。
   - 安装完成后，重启Zotero应用程序。

## 使用方法
1. **启用插件**：确保插件已在Zotero中启用。
2. **翻译文本**：
   - 打开一个PDF文档。
   - 选中需要翻译的文本。
   - 插件会自动显示翻译结果。

## 贡献指南
欢迎大家贡献代码和提出建议，具体步骤如下：
1. **Fork本仓库**：点击右上角的“Fork”按钮，创建一个自己的副本。
2. **克隆仓库**：将Fork后的仓库克隆到本地。
   ```bash
   git clone https://github.com/your-username/repository-name.git
   ```
3. **创建分支**：在本地创建一个新的分支进行开发。
   ```bash
   git checkout -b new-feature
   ```
4. **提交更改**：完成开发后，提交更改并推送到自己的仓库。
   ```bash
   git add .
   git commit -m "添加新功能"
   git push origin new-feature
   ```
5. **发起Pull Request**：在GitHub上发起Pull Request，等待审核和合并。

## 许可证
本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。

## 联系我们
如有任何问题或建议，请通过以下方式联系我们：
- 邮箱：[your-email@example.com]
- GitHub Issues：[在此提出问题](https://github.com/your-username/repository-name/issues)

感谢您的支持与贡献！